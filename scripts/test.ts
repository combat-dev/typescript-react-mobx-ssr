import * as jest from 'jest';

process.env.BABEL_ENV = 'test';
process.env.NODE_ENV = 'test';
process.env.PUBLIC_URL = '';

process.on('unhandledRejection', (e) => {
    throw e;
});

require('../config/env');

const argv = process.argv.slice(2);

if (!process.env.CI && argv.includes('--coverage') === false) {
    argv.push('--watch');
}

jest.run(argv);
