import depcheck from 'depcheck';
import { logMessage } from './utils';

const options: depcheck.Options = {
    ignoreBinPackage: false,
    skipMissing: false,
    ignoreDirs: ['dist', 'build'],
    ignoreMatches: [
        'eslint-*',
        'stylelint-*',
        '@babel/*',
        'babel-*',
        '@types/*',
        '@typescript-eslint/*',
        'husky',
        'lint-staged',
        'install-deps-postmerge',
        'style-loader',
        '@svgr/webpack',
    ],
    parsers: {
        '*.js': depcheck.parser.es6,
        '*.jsx': depcheck.parser.jsx,
        '*.ts': depcheck.parser.typescript,
        '*.tsx': depcheck.parser.typescript,
    },
    detectors: [depcheck.detector.requireCallExpression, depcheck.detector.importDeclaration],
    specials: [depcheck.special.eslint, depcheck.special.webpack],
};

depcheck(`${__dirname}/..`, options, (unused) => {
    logMessage(unused, 'info');
});
