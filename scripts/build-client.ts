import webpack from 'webpack';
import rimraf from 'rimraf';
import chalk from 'chalk';
import getConfig from '../config/webpack.config.ts';
import paths from '../config/paths';
import { logMessage, compilerPromise } from './utils';

const webpackConfig = getConfig(process.env.NODE_ENV || 'development');

const build = async () => {
    rimraf.sync(paths.clientBuild);
    rimraf.sync(paths.serverBuild);

    const [clientConfig] = webpackConfig;
    const webpackCompiler = webpack([clientConfig]);

    const clientCompiler = webpackCompiler.compilers.find((c) => c.name === 'client');
    const clientPromise = compilerPromise('client', clientCompiler);

    // eslint-disable-next-line security/detect-non-literal-fs-filename
    clientCompiler.watch({}, (error, stats) => {
        if (!error && !stats.hasErrors()) {
            logMessage(stats.toString(clientConfig.stats), 'info');
            return;
        }
        logMessage(stats.compilation.errors, 'error');
    });

    try {
        await clientPromise;
        logMessage('Done!', 'info');
        process.exit();
    } catch (error) {
        logMessage(error, 'error');
    }
};

build();
