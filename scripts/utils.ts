import chalk from 'chalk';
import webpack from 'webpack';

type logLevel = 'info' | 'warning' | 'error';
export const logMessage = (message: any, level?: logLevel) => {
    const color =
        level === 'error'
            ? 'red'
            : level === 'warning'
            ? 'yellow'
            : level === 'info'
            ? 'blue'
            : 'white';
    // eslint-disable-next-line security/detect-object-injection
    console.log(`[${new Date().toISOString()}]`, chalk[color](message));
};

export const compilerPromise = (name: string, compiler: webpack.Compiler) => {
    return new Promise((resolve, reject) => {
        compiler.hooks.compile.tap(name, () => {
            logMessage(`[${name}] Compiling`, 'info');
        });

        compiler.hooks.done.tap(name, (stats) => {
            if (!stats.hasErrors()) {
                return resolve();
            }

            return reject(`Failed to compile ${name}`);
        });
    });
};

export const sleep = (ms: number) => new Promise((resolve) => setTimeout(resolve, ms));

export const clientOnly = () => process.argv.includes('--client-only');

export default {
    clientOnly,
    compilerPromise,
    logMessage,
    sleep,
};
