import fs from 'fs';
import webpack from 'webpack';
import rimraf from 'rimraf';
import chalk from 'chalk';
import nodemon from 'nodemon';
import puppeteer from 'puppeteer';

import { choosePort } from 'react-dev-utils/WebpackDevServerUtils';
import getConfig from '../config/webpack.config.ts';
import paths from '../config/paths';
import { logMessage, compilerPromise, sleep } from './utils';

const webpackConfig = getConfig(process.env.NODE_ENV || 'development');

const HOST = process.env.HOST || 'http://localhost';

const generateStaticHTML = async () => {
    const PORT = await choosePort('localhost', 8505);

    process.env.PORT = String(PORT);

    const script = nodemon({
        script: `${paths.serverBuild}/server.js`,
        ignore: ['*'],
    });

    script.on('start', async () => {
        try {
            await sleep(2000);
            const browser = await puppeteer.launch({
                headless: true,
                args: ['--no-sandbox', '--disable-setuid-sandbox'],
            });

            const page = await browser.newPage();
            await page.goto(`${HOST}:${PORT}`);
            const pageContent = await page.content();
            // eslint-disable-next-line security/detect-non-literal-fs-filename
            fs.writeFileSync(`${paths.clientBuild}/index.html`, pageContent);
            await browser.close();
            script.emit('quit');
        } catch (error) {
            script.emit('quit');
            console.error(error);
        }
    });

    script.on('exit', (code) => {
        process.exit(code);
    });
};

const build = async () => {
    rimraf.sync(paths.clientBuild);
    rimraf.sync(paths.serverBuild);

    const [clientConfig, serverConfig] = webpackConfig;
    const multiCompiler = webpack([clientConfig, serverConfig]);

    const clientCompiler = multiCompiler.compilers.find((c) => c.name === 'client');
    const serverCompiler = multiCompiler.compilers.find((c) => c.name === 'server');

    const clientPromise = compilerPromise('client', clientCompiler);
    const serverPromise = compilerPromise('server', serverCompiler);

    // eslint-disable-next-line security/detect-non-literal-fs-filename
    serverCompiler.watch({}, (error, stats) => {
        if (!error && !stats.hasErrors()) {
            console.log(stats.toString(serverConfig.stats));
            return;
        }
        console.error(chalk.red(stats.compilation.errors));
    });

    // eslint-disable-next-line security/detect-non-literal-fs-filename
    clientCompiler.watch({}, (error, stats) => {
        if (!error && !stats.hasErrors()) {
            console.log(stats.toString(serverConfig.stats));
            return;
        }
        console.error(chalk.red(stats.compilation.errors));
    });

    try {
        await serverPromise;
        await clientPromise;
        await generateStaticHTML();
        logMessage('Done!', 'info');
    } catch (error) {
        logMessage(error, 'error');
    }
};

build();
