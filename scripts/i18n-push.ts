import { promises as fs } from 'fs';
import path from 'path';
import glob from 'glob';
import axios, { AxiosError } from 'axios';
import paths from '../config/paths';
import { logMessage } from './utils';

interface LokaliseResponse {
    // eslint-disable-next-line camelcase
    project_id: string;
    file: string;
    result: {
        skipped: number;
        inserted: number;
        updated: number;
    };
}

const upload = async () => {
    const files = glob.sync(path.join(paths.locales, process.env.SOURCE_LANGUAGE, '**/*.json'));
    const responses = await Promise.all(files.map((f) => uploadFile(f)));

    (responses || []).forEach((r) => {
        logMessage(`[${r.file}]\n${JSON.stringify(r.result, null, 2)}`, 'info');
    });
};

const uploadFile = async (filename: string) => {
    // eslint-disable-next-line security/detect-non-literal-fs-filename
    const data = await fs.readFile(filename);

    try {
        const { data: imported } = await axios.post<LokaliseResponse>(
            `https://api.lokalise.co/api2/projects/${process.env.LOKALISE_PROJECT_ID}/files/upload`,
            {
                convert_placeholders: true,
                data: Buffer.from(data).toString('base64'),
                detect_icu_plurals: true,
                filename: path.basename(filename.replace('.missing', '')),
                lang_iso: process.env.SOURCE_LANGUAGE,
                replace_modified: false,
            },
            {
                headers: {
                    'content-type': 'application/json',
                    'X-Api-Token': process.env.LOKALISE_TOKEN,
                },
            }
        );
        return imported;
    } catch (error) {
        logMessage(error, 'error');

        if (error.response && typeof error.response !== 'undefined') {
            const { response } = error as AxiosError;
            logMessage({
                file: path.basename(filename),
                status: response.status,
                statusText: response.statusText,
            });
        }
    }
};

upload();
