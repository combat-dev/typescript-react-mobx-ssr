import { InitialState } from './initial-state-type';

export interface StoreInterface<T = any> {
    setInitialState(initialState: InitialState<T>): void;
}
