// import { MetaDataStore } from '../meta-data.store';
// import { GridStore } from '../grid.store';
import { InitialState } from './initial-state-type';
import { AppStore } from './app';

export interface RootStoreInitialState {
    AppStore: InitialState<AppStore>;
}
