import { observable, toJS } from 'mobx';
import { RootStoreInitialState } from './root-store-initial-state-interface';
import { AppStore } from './app';

export interface Stores {
    AppStore: AppStore;
}

export default class RootStore {
    @observable public stores: Stores;

    constructor(initialState?: RootStoreInitialState) {
        this.stores = {
            AppStore: new AppStore(),
        };

        if (initialState) {
            this.hydrateStores(initialState);
        }
    }

    public hydrateStores(state: RootStoreInitialState) {
        for (const key in state) {
            if (this.stores[key as keyof Stores]) {
                this.stores[key as keyof Stores].setInitialState(
                    state[key as keyof RootStoreInitialState]
                );
            } else {
                throw new Error(`Store with key: ${key} does not exist.`);
            }
        }
    }

    public getState() {
        return toJS(this.stores, { recurseEverything: false, detectCycles: true });
    }
}
