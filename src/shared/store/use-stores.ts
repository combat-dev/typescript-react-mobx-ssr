import React from 'react';
import { MobXProviderContext } from 'mobx-react';
import { Stores } from '.';

export default function useStores(): Stores {
    const rootStore = React.useContext(MobXProviderContext) as Stores;
    return rootStore;
}
