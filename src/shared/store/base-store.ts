import { StoreInterface } from './store-interface';
import { InitialState } from './initial-state-type';

export abstract class BaseStore<T> implements StoreInterface {
    public setInitialState(initialState: InitialState<T>) {
        Object.assign(this, initialState);
    }
}
