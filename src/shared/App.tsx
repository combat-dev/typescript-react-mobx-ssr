import React from 'react';
import { Helmet } from 'react-helmet-async';
import { useTranslation } from 'react-i18next';
import { Link, Route, Switch } from 'react-router-dom';
import favicon from './assets/favicon.png';
import { ReactComponent as ReactLogo } from './assets/react.svg';

import styles from './App.module.scss';
import routes from './routes';
import Home from './Pages/Home';
import Page1 from './Pages/Page1';
import Page2 from './Pages/Page2';

const defaultTitle = 'React MobX SSR Starter - Typescript edition';

const App: React.FC = () => {
    const { t } = useTranslation();
    return (
        <div className={styles.wrapper}>
            <Helmet
                defaultTitle={defaultTitle}
                titleTemplate={`%s - ${defaultTitle}`}
                link={[{ rel: 'icon', type: 'image/png', href: favicon }]}
            />
            <h1>
                <ReactLogo className={styles.reactLogo} /> React + Express + MobX - SSR Starter -
                Typescript Edition
            </h1>
            <Switch>
                <Route exact path={routes.home} component={Home} />
                <Route exact path={routes.page1} component={Page1} />
                <Route exact path={routes.page2} component={Page2} />
                <Route render={() => '404!'} />
            </Switch>
            <h2>{t('router-headline')}</h2>
            <ul>
                <li>
                    <Link to="/">{t('nav.home')}</Link>
                </li>
                <li>
                    <Link to="/page-1">{t('nav.page-1')}</Link>
                </li>
                <li>
                    <Link to="/page-2">{t('nav.page-2')}</Link>
                </li>
            </ul>
        </div>
    );
};

export default App;
