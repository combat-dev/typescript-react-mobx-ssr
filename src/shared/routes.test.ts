import { getRoute } from './routes';

const testRoutes = {
    home: '/',
    users: {
        list: '/users',
        view: '/users/:id',
    },
    pages: {
        view: '/pages/:id-:slug',
    },
};

describe('routes#getRoute', () => {
    it('should return a simple route', () => {
        expect(getRoute('home', undefined, testRoutes)).toEqual('/');
    });

    it('should return a nested route', () => {
        expect(getRoute('users.list', undefined, testRoutes)).toEqual('/users');
    });

    it('should return a route with its parameter intact if no parameter was given', () => {
        expect(getRoute('users.view', undefined, testRoutes)).toEqual('/users/:id');
    });

    it('should return a route with its parameter replaced', () => {
        expect(getRoute('users.view', { id: 1 }, testRoutes)).toEqual('/users/1');
    });

    it('should return a rout with multiple parameters replaced', () => {
        expect(getRoute('pages.view', { id: 1, slug: 'test' }, testRoutes)).toEqual(
            '/pages/1-test'
        );
    });

    it('should return undefined if a route is not found', () => {
        expect(getRoute('does.not.exist')).toBeUndefined();
    });
});
