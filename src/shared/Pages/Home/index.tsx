import React, { useCallback, Fragment } from 'react';
import { useTranslation } from 'react-i18next';
import { observer } from 'mobx-react';
import useStores from '../../store/use-stores';
import { Locale } from '../../store/app/types';
import Features from '../../components/Features';

const Home = observer(() => {
    const { t } = useTranslation();
    const { AppStore } = useStores();

    const handleLocaleChange = useCallback(
        (e: React.FormEvent<HTMLButtonElement>) => {
            AppStore.setLocale(e.currentTarget.value as Locale);
        },
        [AppStore]
    );

    return (
        <Fragment>
            <Features />
            <h2>{t('i18n-example')}</h2>
            <p>
                <button
                    value="nl_NL"
                    disabled={AppStore.locale === 'nl_NL'}
                    onClick={handleLocaleChange}
                >
                    Nederlands
                </button>
                <button
                    value="en_US"
                    disabled={AppStore.locale === 'en_US'}
                    onClick={handleLocaleChange}
                >
                    English
                </button>
            </p>
        </Fragment>
    );
});

export default Home;
