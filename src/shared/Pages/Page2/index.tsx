import React from 'react';
import styles from './Page2.scss';

const Page2 = () => <div className={styles.wrapper}>Page 2</div>;

export default Page2;
