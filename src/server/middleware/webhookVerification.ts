import * as express from 'express';

const webhookVerification = (
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
) => {
    if (req.query.webhookToken !== process.env.WEBHOOK_TOKEN) {
        return res.sendStatus(403);
    }

    return next();
};

export default webhookVerification;
