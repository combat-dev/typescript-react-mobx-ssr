import * as express from 'express';
import RootStore from '../../shared/store';

const addStores = (_req: express.Request, res: express.Response, next: express.NextFunction) => {
    const rootStore = new RootStore();
    res.locals.stores = rootStore.stores;

    next();
};

export default addStores;
