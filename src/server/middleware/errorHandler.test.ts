import { Request, Response } from 'express';

import errorHandler from './errorHandler';

describe('middleware#errorHandler', () => {
    it('should handle a 404 errors', () => {
        const res: Partial<Response> = {};
        const req: Partial<Request> = {};
        res.status = (code: number) => {
            res.statusCode = code;
            return res as Response;
        };

        res.json = jest.fn();

        const next = jest.fn();
        const err = new Error('Test');
        errorHandler(err, req as any, res as any, next);

        expect(res.statusCode).toBe(404);
        expect(res.json).toBeCalled();
    });
});
