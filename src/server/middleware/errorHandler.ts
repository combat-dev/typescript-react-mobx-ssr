import path from 'path';
import { Request, Response, NextFunction } from 'express';

const errorHandler = (err: Error, _req: Request, res: Response, _next: NextFunction) =>
    res.status(404).json({
        status: 'error',
        message: err.message,
        stack:
            process.env.NODE_ENV === 'development' &&
            (err.stack || '')
                .split('\n')
                .map((l) => l.trim())
                .map((l) => l.split(path.sep).join('/'))
                .map((l) => l.replace(process.cwd().split(path.sep).join('/'), '.')),
    });

export default errorHandler;
