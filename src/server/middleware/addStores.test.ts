import express from 'express';
import RootStore, { Stores } from '../../shared/store';
import addStores from './addStores';

const rootStore = new RootStore();
describe('middleware#addStores', () => {
    it('should create clean stores and add them to the express response', () => {
        const req: Partial<express.Request> = {};
        const res: Partial<express.Response<{ locals: { stores?: Stores } }>> = {
            locals: {},
            status: jest.fn(),
        };
        const next = jest.fn();
        addStores(req as any, res as any, next);

        expect(res.locals.stores).toBeDefined();
        expect(res.locals.stores).toEqual(rootStore.stores);
        expect(next).toBeCalled();
    });
});
