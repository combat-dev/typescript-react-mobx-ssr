import fs from 'fs';
import path from 'path';
import * as express from 'express';
import { Locale } from '../../shared/store/app/types';

type TranslationCache = {
    [locale: string]: {
        [ns: string]: {
            values: string;
            updatedAt: number;
        };
    };
};

const translationCache: TranslationCache = {};

const localesDir = path.join(__dirname, 'locales');

const isCached = (locale: Locale, ns: string) =>
    !!translationCache[`${locale}`] && !!translationCache[`${locale}`][`${ns}`];

const isOutdated = (locale: Locale, ns: string) =>
    isCached(locale, ns) &&
    translationCache[`${locale}`][`${ns}`].updatedAt <
        // eslint-disable-next-line security/detect-non-literal-fs-filename
        new Date(fs.statSync(path.resolve(`${localesDir}/${locale}/${ns}.json`)).mtime).getTime();

const loadAndCache = (locale: Locale, ns: string) => {
    translationCache[`${locale}`] = {
        [ns]: {
            // eslint-disable-next-line security/detect-non-literal-fs-filename
            values: fs.readFileSync(`${localesDir}/${locale}/${ns}.json`, { encoding: 'utf-8' }),
            updatedAt: new Date(
                // eslint-disable-next-line security/detect-non-literal-fs-filename
                fs.statSync(path.resolve(`${localesDir}/${locale}/${ns}.json`)).mtime
            ).getTime(),
        },
    };
};

const getTranslation = (locale: Locale, ns: string) => translationCache[`${locale}`][`${ns}`];

export const i18nextXhr = (
    req: express.Request<{ locale: Locale; ns: string }>,
    res: express.Response
) => {
    const { locale, ns } = req.params;
    try {
        if (isCached(locale, ns) === false || isOutdated(locale, ns) === true) {
            loadAndCache(locale, ns);
        }

        const { values, updatedAt } = getTranslation(locale, ns);
        return res.header('Last-Modified', new Date(updatedAt).toUTCString()).send(values);
    } catch (error) {
        console.log(error.message);
        return res.send(null);
    }
};

export const refreshTranslations = async (_req: express.Request, res: express.Response) => {
    const { download, writeFiles, cleanup } = require('../lib/i18n/lokalise');

    const data = await download();
    await writeFiles(data, `${__dirname}/locales`);
    cleanup();
    res.sendStatus(201);
};

export default i18nextXhr;
