import React from 'react';
import * as express from 'express';
import { renderToString } from 'react-dom/server';
import { StaticRouter as Router } from 'react-router-dom';
import { useStaticRendering as staticRendering, Provider } from 'mobx-react';
import { HelmetProvider } from 'react-helmet-async';

import IntlProvider from '../../shared/i18n/IntlProvider';
import App from '../../shared/App';
import Html from '../components/Html';

const helmetContext: any = {};
const routerContext = {};

const serverRenderer = (req: express.Request, res: express.Response) => {
    staticRendering(true);

    const content = renderToString(
        <Provider {...res.locals.stores}>
            <Router location={req.url} context={routerContext}>
                <IntlProvider>
                    <HelmetProvider context={helmetContext}>
                        <App />
                    </HelmetProvider>
                </IntlProvider>
            </Router>
        </Provider>
    );

    const state = JSON.stringify(res.locals.stores);

    return res.send(
        '<!doctype html>' +
            renderToString(
                <Html
                    css={[res.locals.assetPath('bundle.css'), res.locals.assetPath('vendor.css')]}
                    helmetContext={helmetContext}
                    scripts={[res.locals.assetPath('bundle.js'), res.locals.assetPath('vendor.js')]}
                    state={state}
                    locale={res.locals.stores.AppStore.locale}
                >
                    {content}
                </Html>
            )
    );
};

export default serverRenderer;
