import fs from 'fs';
import path from 'path';
import mkdirp from 'mkdirp';
import rimraf from 'rimraf';
import decompress from 'decompress';
import axios from 'axios';
import glob from 'glob';
import paths from '../../../../config/paths';

if (!process.env.LOKALISE_TOKEN || !process.env.LOKALISE_PROJECT_ID) {
    throw new Error('Please add lokalise credentials to your .env file');
}

const LOKALISE_PROJECT_URL = `https://api.lokalise.co/api2/projects/${process.env.LOKALISE_PROJECT_ID}`;

export const getTempDir = () => path.join(__dirname, 'tmp');

export const download = async () => {
    try {
        const { data: exported } = await axios.post(
            `${LOKALISE_PROJECT_URL}/files/download`,
            {
                fomat: 'json',
                plural_format: 'i18next',
                placeholder_format: 'i18n',
                original_filenames: false,
                bundle_structure: 'locales/%LANG_ISO%.%FORMAT%',
            },
            {
                headers: {
                    'content-type': 'application/json',
                    'X-Api-Token': process.env.LOKALISE_TOKEN,
                },
            }
        );

        const file = await axios({ url: exported.bundle_url, responseType: 'arraybuffer' });

        return file.data;
    } catch (error) {
        console.error(error);
    }
};

// @ts-ignore
// this is not yet used so we disable the eslint rule
// eslint-disable-next-line @typescript-eslint/no-unused-vars
const collectTranslationSourceFiles = async () => {
    const translationFiles = glob.sync(
        path.join(paths.locales, process.env.SOURCE_LANGUAGE, '**/*.json')
    );

    const responses = await Promise.all(translationFiles.map((file) => uploadTranslations(file)));
    console.log(responses);
};

const uploadTranslations = async (file: string) => {
    // eslint-disable-next-line security/detect-non-literal-fs-filename
    const data = fs.readFileSync(file, { encoding: 'utf-8' });
    try {
        const { data: imported } = await axios.post(
            `${LOKALISE_PROJECT_URL}/files/upload`,
            {
                convert_placeholders: true,
                data,
                detect_icu_plurals: true,
                filename: file.replace('.missing', ''),
                lang_iso: process.env.SOURCE_LANGUAGE,
                replace_modified: false,
            },
            {
                headers: {
                    'content-type': 'application/json',
                    'X-Api-Token': process.env.LOKALISE_TOKEN,
                },
            }
        );

        return imported;
    } catch (error) {
        console.error(error);
    }
};

const upload = async () => {
    const translationFiles = glob.sync(
        path.join(paths.locales, process.env.SOURCE_LANGUAGE, '**/*.json')
    );
    console.log(translationFiles);
};

export const writeFiles = async (data: string, targetFolder: string) => {
    rimraf.sync(getTempDir());
    mkdirp.sync(targetFolder);
    mkdirp.sync(getTempDir());

    const translationsBundle = path.join(getTempDir(), 'locales.zip');

    // eslint-disable-next-line security/detect-non-literal-fs-filename
    fs.writeFileSync(translationsBundle, data);
    await decompress(translationsBundle, getTempDir());

    const files = glob.sync(path.join(getTempDir(), '**.*.json'));
    for (const f of files) {
        // eslint-disable-next-line security/detect-non-literal-fs-filename
        const fileContent = JSON.parse(fs.readFileSync(f, { encoding: 'utf-8' }));
        const locale = path.basename(f, '.json');

        Object.entries(fileContent).forEach(([namespace, values]) => {
            mkdirp.sync(`${targetFolder}/${locale}`);
            // eslint-disable-next-line security/detect-non-literal-fs-filename
            fs.writeFileSync(`${targetFolder}/${locale}/${namespace}`, JSON.stringify(values), {
                encoding: 'utf-8',
            });
        });
    }
};

export const cleanup = () => {
    rimraf.sync(getTempDir());
};

export default {
    cleanup,
    download,
    getTempDir,
    upload,
    writeFiles,
};
