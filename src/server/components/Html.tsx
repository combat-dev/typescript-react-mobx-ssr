import React from 'react';
import { FilledContext } from 'react-helmet-async';
import jsesc from 'jsesc';
import { observer } from 'mobx-react';

interface Props {
    children: string;
    css: string[];
    helmetContext: FilledContext;
    scripts: string[];
    state: string;
    locale: string;
}

const Html = observer(
    ({
        children,
        css = [],
        scripts = [],
        state = '{}',
        helmetContext: { helmet },
        locale,
    }: Props) => {
        return (
            <html lang={locale.substring(0, 2)}>
                <head>
                    <meta charSet="utf-8" />
                    <meta name="viewport" content="width=device-width, initial-scale=1" />
                    {helmet.base.toComponent()}
                    {helmet.title.toComponent()}
                    {helmet.meta.toComponent()}
                    {helmet.link.toComponent()}
                    {helmet.script.toComponent()}
                    {css.filter(Boolean).map((href) => (
                        <link key={href} rel="stylesheet" href={href} />
                    ))}
                    <script
                        // eslint-disable-next-line react/no-danger
                        dangerouslySetInnerHTML={{
                            __html: `window.__PRELOADED_STATE__ = ${jsesc(state, {
                                json: true,
                                isScriptContext: true,
                                wrap: true,
                            })}`,
                        }}
                    />
                </head>
                <body>
                    {/* eslint-disable-next-line react/no-danger */}
                    <div id="app" dangerouslySetInnerHTML={{ __html: children }} />
                    {scripts.filter(Boolean).map((src) => (
                        <script key={src} src={src} />
                    ))}
                </body>
            </html>
        );
    }
);

export default Html;
