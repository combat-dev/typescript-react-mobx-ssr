const babelToolingConfig = require('./babel.config').env.tooling;

require('@babel/register')({
    ...babelToolingConfig,
    extensions: ['.js', '.jsx', '.ts', '.tsx'],
});

module.exports = require('./.eslintrc.ts').default;
