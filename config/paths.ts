import path from 'path';
import fs from 'fs';

const appDirectory = fs.realpathSync(process.cwd());
const resolvePath = (relativePath: string) => path.resolve(appDirectory, relativePath);

const resolvedPaths = {
    appHtml: resolvePath('config/webpack.config.ts/template.html'),
    clientBuild: resolvePath('build/client'),
    serverBuild: resolvePath('build/server'),
    dotenv: resolvePath('.env'),
    src: resolvePath('src'),
    srcClient: resolvePath('src/client'),
    srcServer: resolvePath('src/server'),
    srcShared: resolvePath('src/shared'),
    types: resolvePath('node_modules/@types'),
    locales: resolvePath('src/shared/i18n/locales'),
    publicPath: '/static/',
};

const resolveModules = [
    resolvedPaths.srcClient,
    resolvedPaths.srcServer,
    resolvedPaths.srcShared,
    resolvedPaths.src,
    'node_modules',
];

const paths = {
    ...resolvedPaths,
    resolveModules,
};

export default paths;
