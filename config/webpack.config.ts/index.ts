import webpack from 'webpack';
import clientProdConfig from './client.prod';
import clientDevConfig from './client.dev';
import serverProdConfig from './server.prod';
import serverDevConfig from './server.dev';

export default (env = 'production') => {
    if (env === 'development' || env === 'dev') {
        process.env.NODE_ENV = 'development';
        return [require('./client.dev').default, require('./server.dev').default];
    }
    process.env.NODE_ENV = 'production';
    return [require('./client.prod').default, require('./server.prod').default];
};
