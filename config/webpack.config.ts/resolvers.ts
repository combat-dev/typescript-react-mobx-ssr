import path from 'path';
import paths from '../paths';

const getDependencyPath = (name: string) => path.join(__dirname, '..', '..', 'node_modules', name);

export default {
    extensions: ['.js', '.json', '.jsx', '.ts', '.tsx', '.css'],
    modules: paths.resolveModules,
    alias: {
        'react': require.resolve('react'),
        'react-dom': require.resolve('react-dom'),
        'react-router': getDependencyPath('react-router'),
        'react-router-dom': getDependencyPath('react-router-dom'),
        'react-i18next': getDependencyPath('react-i18next'),
        'i18next': getDependencyPath('i18next'),
    },
};
